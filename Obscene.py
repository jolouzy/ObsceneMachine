import re
import transliterate
import logging

class ObsceneMachine:
    def __init__(self):
        logging.basicConfig(level=logging.INFO)
        logging.info("ObsceneMachine loaded.")
        self.regex = re.compile(r'\b(((у|[нз]а|(хитро|не)?вз?[ыьъ]|с[ьъ]|(и|ра)[зс]ъ?|(о[тб]|п[оа]д)[ьъ]?|(.\B)+?[оаеи-])-?)?([её](б(?!о[рй]|рач)|п[уа](ц|тс))|и[пб][ае][тцд][ьъ]).*?|((н[иеа]|ра[зс]|[зд]?[ао](т|дн[оа])?|с(м[еи])?|а[пб]ч)-?)?ху([яйиеёю]|л+и(?!ган)).*?|бл([эя]|еа?)([дт][ьъ]?)?|\S*?(п([иеё]зд|ид[аое]?р|ед(р(?!о)|[аое]р|ик))|бля([дбц]|тс)|[ое]ху[яйиеё]|хуйн|охую).*?|(о[тб]?|про|на|вы)?м(анд([ауеыи](л(и[сзщ])?[ауеиы])?|ой|[ао]в.*?|юк(ов|[ауи])?|е[нт]ь|ища)|уд([яаиое].+?|е?н([ьюия]|ей))|[ао]л[ао]ф[ьъ]([яиюе]|[еёо]й))|елд[ауые].*?|ля[тд]ь|([нз]а|по)х)\b', re.IGNORECASE)
    
    def check_text(self, text):
        russian_text = text
        english_text = transliterate.translit(text, 'ru')
        russian_match = self.regex.search(russian_text)
        english_match = self.regex.search(english_text)
        
        if russian_match or english_match:
            return True
        else:
            return False