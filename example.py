from Obscene import ObsceneMachine

machine = ObsceneMachine()
text = input("Enter your message: ")
if machine.check_text(text):
    print("The message contains obscene words.")
else:
    print("The message doesn't contain obscene words.")